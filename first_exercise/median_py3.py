# script for Python 3
# supports versions before v3.4 and later, because `statistics` lib with `median` function only appeared in v3.4
# https://docs.python.org/3/library/statistics.html

__author__ = 'Maxim Dutkin'


def do_the_job():
    try:
        from statistics import median
    except ImportError:
        # if there is no `statistics` lib yet, define `median` func
        def median(data):
            """
            Returns median value of array. If count of elements is odd - returns the middle data point, in the even case
            returns the average of the two middle values.
            :param data: array
            :return: median value
            """
            data = sorted(data)
            n = len(data)
            if n == 0:
                raise ValueError("No median for empty data")
            if n % 2 == 1:
                return data[n // 2]
            else:
                i = n // 2
                return (data[i - 1] + data[i]) / 2

    first_arr = []
    print('Please, input numbers you want to see in FIRST ARRAY (an element per row).\n'
          'Input of empty string or ";" char will finish the input of array.')
    while True:
        input_str = input('Input a number: ')
        try:
            if len(input_str) == 0 or input_str == ';':
                if len(first_arr) == 0:
                    print('An array can\'t be empty, continue your input.')
                    continue
                print('Input of FIRST ARRAY is finished, it is: {0}\n'.format(first_arr))
                break

            number = float(input_str)
            first_arr.append(number)
        except ValueError:
            print("Invalid number, repeat your input. (Current array is {0})".format(first_arr))

    second_arr = []
    print('Please, input numbers you want to see in SECOND ARRAY (an element per row).\n'
          'Input of empty string or ";" char will finish the input of array.')
    while len(second_arr) != len(first_arr):
        input_str = input('Input a number: ')
        try:
            if len(input_str) == 0 or input_str == ';':
                if len(first_arr) == 0:
                    print('An array can\'t be empty, continue your input.')
                    continue
                if len(first_arr) > len(second_arr):
                    print('Two arrays must be the same size, continue your input')
                    continue
                print('Input of SECOND ARRAY is finished, it is: {0}\n'.format(first_arr))
                break

            number = float(input_str)
            second_arr.append(number)
        except ValueError:
            print("Invalid number, repeat your input. (Current array is {0})".format(first_arr))
            continue

    sum_arr = first_arr[:]
    sum_arr.extend(second_arr)
    print('Both arrays are entered:'
          '\n\t{0}'
          '\n\t{1}'
          '\nArray of 2N-length is:'
          '\n\t{2}'
          '\nThe median of this 2N-array will be:'
          '\n\t{3}'.
          format(first_arr, second_arr, sum_arr, median(sum_arr)))