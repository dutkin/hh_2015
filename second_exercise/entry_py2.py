__author__ = 'Maxim Dutkin'

from collections import deque
import random
import string

_sentinel = object()


def next(it, default=_sentinel):
    """
    This func provides compatibility with Python 3
    :param it: iterator
    :param default: default value, when iterator ends
    :return: next value or default
    """
    try:
        return it.next()
    except StopIteration:
        if default is _sentinel:
            raise
        return default


def random_char(y):
    """
    Returns random string with `y` length in lower case
    :param y: Length of a string
    :return: `str` of random chars
    """
    return ''.join(random.choice(string.ascii_letters) for x in range(y)).lower()


def str_seq():
    """
    Infinite sequence for testing purposes. The beginning of it is defined, so we can manually count entry index,
    for example, for `mn` sequence

    c d m m m m m n n n  n  a  d  a  d  s  a  .......
    1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 .......

    so, the right `find_entry()` result for `mn` will be `7`
    :return: Yields a char per iteration
    """
    for char in 'cdmmmmmnnnnadadsa':
        yield char
    while True:
        yield random_char(1)


def sequence():
    """
    Func is a `generator` of chars, made from positive numbers, started from `1`, incremented by `1` and returned
    one by one, i.e.:
    1
    2
    3
    4
    5
    ....
    making the sequence: 123456789101112131415....
    As it is `generator`, it's possible to make an infinite loop inside, generating a really infinite sequence and
    not holding the main thread.
    :return: Yields a char per iteration
    """
    i = 0
    while True:
        i += 1
        for char in str(i):
            yield char


def search_in_buf(buf, seq, input_seq):
    """
    Func scans the given buffer for entries of `input_seq`, and if founds them - puts matches into new buffer and
    makes recursive call to itself with new buffer to scan it too
    :param buf: Buffer of `deque` type. Each element of this buffer is a `list` with a format:
    `(current_char, its_index)`
    :param seq: Reference to sequence we are searching in
    :param input_seq: Sequence, which entry we are searching for
    :return: `list` with a format: `(index_of_the_first_match, current_sequence_index)`. If no entry was found,
    `index_of_the_first_match` is `-1`
    """
    seq_index = 0
    tmp_buf = deque()
    while len(buf):
        cur_char, seq_index = buf.popleft()
        first_match_index = seq_index
        matches_count = 0
        for input_seq_char in input_seq:
            if cur_char == input_seq_char:
                # take next value - from buf if it's not empty, or iterating through `seq`
                if len(buf) == 0:
                    cur_char = next(seq)
                    seq_index += 1
                else:
                    cur_char, seq_index = buf.popleft()
                matches_count += 1

                # in the case of positive match - add values to new buf to make a recursive call of `search_in_buf()`
                # later
                tmp_buf.append((cur_char, seq_index))
            else:
                break

        if matches_count == len(input_seq):
            # if we've reached this point - we have total match and can return the result
            return first_match_index, seq_index
        if len(tmp_buf) != 0:
            # if temporary buf is not empty, make a recursive call to scan it too
            # return search_in_buf(tmp_buf, seq, input_seq)
            buf_result = search_in_buf(tmp_buf, seq, input_seq)
            if buf_result[0] == -1:
                # no entry in tmp_buf, continue with last taken seq_index
                seq_index = buf_result[1]
            else:
                return buf_result

    # looped through all possible variants in buf, no entry found
    return -1, seq_index


def find_entry(inf_seq, input_seq):
    """
    Finds index value of the first entry of `input_seq` into `inf_seq`, numeration starts from 1
    :param inf_seq: `generator`, used to generate infinite sequence
    :param input_seq: This is `str` sequence, for which we are looking first entry
    :return: `int` index of the first entry
    """
    seq_index = 0
    buf = deque()
    first_match_index = -1

    for cur_char in inf_seq:
        seq_index += 1

        # if first digit matches - remember `seq_index` - it may be the result
        first_match_index = seq_index
        matches_count = 0
        for input_seq_char in input_seq:
            if cur_char == input_seq_char:
                seq_index += 1
                cur_char = next(inf_seq)
                # add `cur_char` and it's index to buffer as (cur_char, seq_index) to iterate through it later,
                # if current iteration fails
                buf.append((cur_char, seq_index))
                matches_count += 1
            else:
                break

        # if we looped through all chars in `input_seq` with positive match - we found our sequence, and
        # `first_match_index` is really the result
        if matches_count == len(input_seq):
            break

        if len(buf) != 0:
            # iterate through `buf`, check if there is any matches in it
            buf_result = search_in_buf(buf, inf_seq, input_seq)

            if buf_result[0] == -1:
                # `-1` means that we didn't find entry, continue with the last `seq_index`
                seq_index = buf_result[1]
            else:
                # entry was found!
                return buf_result[0]
            buf = deque()

    return first_match_index


def perform_some_tests():
    print('Test 1.\n'
          'We generate a long string to be able to check our result with Python system function `find()`, then\n'
          'check results for all numbers in range 0-1000.')
    errors = 0
    very_long_str = ''
    some_seq = sequence()
    i = 0
    for char in some_seq:
        i += 1
        if i == 10000:
            break
        very_long_str += char

    print('Very long string: %s' % very_long_str)
    for l in range(0, 1000):
        find_entry_result = find_entry(sequence(), str(l))
        find_result = very_long_str.find(str(l))
        if find_entry_result != find_result + 1:
            errors += 1
            print('\tEntered_seq=%s, find_entry_result=%s, find_result=%s' % (l,
                                                                              find_entry_result,
                                                                              find_result + 1))
    print('Errors in Test 1: %s' % errors)
    raw_input('Press <Enter> to continue...')

    print('Test 2.\n'
          'Now it\'s time to check some pre-defined values.')
    errors = 0
    pre_def = ('6789',
               '111',
               '12',
               '10',
               '100',
               '909192939495',
               '555',
               '55556', )
    print('Sequences to check: %s' % [x for x in pre_def])
    for seq in pre_def:
        find_entry_result = find_entry(sequence(), seq)
        find_result = very_long_str.find(seq)
        print('\tEntered_seq=%s, find_entry_result=%s, find_result=%s' % (seq,
                                                                          find_entry_result,
                                                                          find_result + 1))
        if find_entry_result != find_result + 1:
            errors += 1
    print('Errors in Test 2: %s' % errors)
    raw_input('Press <Enter> to continue...')

    print('Test 3.\n'
          'Test a sequence with pre-defined beginning, so we can count entry index manually:\n\t'
          'c d m m m m m n n n  n  a  d  a  d  s  a  .......\n\t'
          '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 .......')
    print('Sequences to check: %s' % 'mn')
    print('\tfind_entry_result=%s, find_result=%s' % (find_entry(str_seq(), 'mn'), 'cdmmmmmnnnnadadsa'.find('mn') + 1))


def do_the_job():
    print('It is possible to run this script in a testing mode with some pre-defined input sequences. It also shows,\n'
          'that `find_entry()` supports not only sequence with integers (in the example a random sequence of ascii\n'
          'characters is used).')
    test_mode = False
    while True:
        input_str = raw_input('Enable TEST MODE? (y/n): ')
        try:
            if len(input_str) == 0:
                print('Answer can\'t be empty, repeat your input.')
                continue
            input_str = input_str.lower().strip()
            if input_str not in ('yes', 'y', 'no', 'n'):
                raise ValueError
            if input_str in ('yes', 'y'):
                test_mode = True
                print('"TEST MODE" enabled.')
            break
        except ValueError:
            print('Invalid symbols, repeat your input.')

    if test_mode:
        perform_some_tests()
    else:
        seq_to_search = ''
        print('PLease, enter the sequence you want to search for. Only positive integer numbers are allowed.')
        while True:
            input_str = raw_input('Sequence to search for: ')
            try:
                if len(input_str) == 0:
                    print('Sequence can\'t be empty, repeat your input.')
                    continue
                seq_to_search = input_str.strip()
                # check if it's int, otherwise - throw exc
                int(seq_to_search)
                if seq_to_search[0] == '-':
                    raise ValueError
                break
            except ValueError:
                print('Invalid sequence, repeat your input.')

        print('\tSearching for `%s` sequence, it may take a while...' % seq_to_search)
        result_index = find_entry(sequence(), seq_to_search)
        print('\tIndex number of the first entry into the infinite sequence is: %s' % result_index)