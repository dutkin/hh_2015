# script support bot Python 2.* and Python 3.* versions
__author__ = 'Maxim Dutkin'

import sys


cur_version = sys.version_info[:3]

if cur_version >= (3, 0, 0):
    from second_exercise.entry_py3 import do_the_job
else:
    from second_exercise.entry_py2 import do_the_job

if __name__ == "__main__":
    do_the_job()